#!/bin/bash


verify_parameters()
{
    if [[ -z "$azure_email" ]]; then
        echo 'Specify the Microsoft email to connect with to the Azure Portal'
        exit 0
    fi

    if [[ -z "$azure_password" ]]; then
        echo 'Specify the Microsoft account password'
        exit 0
    fi

    if [[ -z "$vm_name" ]]; then
        echo "Specify the new virtual machine's name"
        exit 0
    fi

    if [[ -z "$vm_password" ]]; then
        echo 'Specify a password to give to the new virtual machine'
        exit 0
    fi
}

download_azure_cli()
{
    sudo apt-get update
    sudo apt-get -y install ca-certificates curl apt-transport-https lsb-release gnupg
    curl -sL https://packages.microsoft.com/keys/microsoft.asc |
    gpg --dearmor |
    sudo tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null
    AZ_REPO=$(lsb_release -cs)
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" |
    sudo tee /etc/apt/sources.list.d/azure-cli.list
    sudo apt-get update
    sudo apt-get install -y azure-cli
}

connect_to_azure_portal()
{
    az login --username $azure_email --password $azure_password
}

create_virtual_machine()
{
    #Microsoft Azure Sponsorship
    subscription='c5250ba7-729c-4adc-8a6a-9c9063295377'

    vm_size='Standard_NV6_Promo'

    # Extract username from azure_email
    vm_admin=$(echo $azure_email | cut -d "@" -f 1)

    image='Canonical:0001-com-ubuntu-server-focal:20_04-lts:20.04.202009070'
   
    resource_group='Metalab'
    location='eastus'

    az vm create --name $vm_name --resource-group $resource_group --image $image --size $vm_size --subscription $subscription --location $location --admin-username $vm_admin --admin-password $vm_password
}

show_virtual_machine_infos()
{
    sudo apt-get -y install jq
    vm_infos=$(az vm show --name $vm_name --resource-group $resource_group --show-detail)
    vm_public_ip=`echo ${vm_infos} | jq -r '.publicIps'`
    echo "Public IP:" $vm_public_ip
}

while (( "$#" )); do
    case "$1" in
        -h|--help)
            echo "$package - create azure server"
            echo " "
            echo "parameters:"
            echo "-h, --help                  show brief help"
            echo "-e, --email                 specify microsoft account email to use"
            echo "-p, --password              specify microsoft account password to use"
            echo "-n, --name                  specify new azure server name"
            echo "-a, --azure-vm-password     specify new azure server password"
            exit 0
        ;;
        -e| --email)
            shift
            azure_email=$1
            shift
        ;;
        -p|--password)
            shift
            azure_password=$1
            shift
        ;;
        -n|--name)
            shift
            vm_name=$1
            shift
        ;;
        -a|--azure-vm-password)
            shift
            vm_password=$1
            shift
        ;;
        *)
          break
        ;;
  esac
done

verify_parameters
download_azure_cli
connect_to_azure_portal
create_virtual_machine
show_virtual_machine_infos
